IEEE 830-1998 Software Requirements Specification

# TITLE: "artENG: an app made to match art and engineering students in their freelance endeavors"

# Table of Contents



## 1. Introduction

### 1.1 Purpose
The purpose of this document is to provide a Software Requirements Specification (SRS) for the development of "artENG", a web application that enables engineering students to hire art students for design, and art students to hire engineering students to implement their designs. This document describes the requirements of the web application from the perspectives of the user, system, and customer.

### 1.2 Document Conventions
This document is written using the IEEE 830-1998 standard for software
requirements specifications. It will include user, system, and customer
requirements for the completion of the project.

### 1.3 Intended Audience and Reading Suggestions
This document is intended for the developers and designers working on the
"artENG" project. It's recommended that readers start from the
beginning and read the document from start to finish to get a complete picture
of the project requirements.

### 1.4 Product Scope
The scope of "artENG" is to create a web application that has two main types of users:
- Engineering students:
	- can post job offers that require the expertise of art students
	- can choose a suitable art student once they apply to the listed offering
	- can apply for jobs listed by art students
    - at application, engineering students can give their quotes for the job
- Art students:
	- can post job offers that require the expertise of engineering students
	- can choose a suitable engineering student once they apply to the listed offering
	- can apply to jobs listed by engineering students
    - at application, art students can give their quotes for the job

Additionally, "artENG" will offer a reliable and secure payment system as well as a review system that will ensure a trusting and fair environment.

### 1.5 References
1. IEEE 830-1998: IEEE Standard for Software Requirements Specifications (IEEE,
1998).
2. Django 5.0 documentation

### 1.6 Overview
The "artENG" web application will be a platform for both engineering and art students to post jobs (also referred to as "tasks" in this document), and for correspondents to submit quotes. The application will also provide a secure payment system and a review system for users to rate their experience with students.

## 2. Overall Description

### 2.1 Product perspective
"artENG" is a web application tailored for art and engineering students to efficiently exchange freelance tasks, enabling them to collaborate on short-term tasks.

The user interface is designed to be intuitive and easy to use, with an Admin dashboard, Student dashboard, and a public part with a list of both art and engineering students, and a list of available jobs filterable by their nature. Users can click on particular students profile and find out their past record, as well as their institution and study programme information. 

Students can post jobs and receive quotes from other students. After the poster selects the student they deem right for their tasks, they can deliver the entire specification through the app. The poster pays full price upon selecting the worker, however, the worker receives compensation after completing the task. In the meantime, funds are kept by "artENG" to ensure there are no unnecessary transaction fees in case of a dispute.

Disputes are handled by two admins. One admin represents engineering students and has the skill set necessary to objectively rate the work of engineering students. Other admin represent art students and has the corresponding skill set. If two admins cannot mutually agree, they are stripped of the case and two new admins are brought in. This process repeats until two admins agree on how to handle the dispute.

### 2.2 Product Functions
"artENG" will provide the following functions:
User Interface: Students will be able to navigate between pages on the web application, communicate with other students, view the names of the students working on their task, and contact a student before assigning a task.
Job Posting and Assignment: Students will be able to post jobs and receive quotes from other students, delete the jobs, and request a specific student for their task.
Job Delivery and Payment: students will be noted when their tasks are completed and the web application will handle any disputes.
Review System: The web application will include a rating system for students, as well as allow users to review the quality of their completed tasks.
Job and Student Search: The web application will provide a searchable database of students.

### 2.3 User Classes and Characteristics
The product will have four logical user classes: Engineering admin, Art admin, Engineering student and Art student. Admin (either Art or Engineering) is a user class with a superuser status, and can access the Admin dashboard. Admin users can create, edit, and delete users.

### 2.3.1 Art Admin 
Art Admin users will be able to manage the web application, including managing users and managing jobs. Admin users will also be able to review disputes and disagreements between students, as long as they are able to form an agreement with an Engineering admin. 


### 2.3.2 Engineering Admin 
Engineering Admin users will be able to manage the web application, including managing users and managing jobs. Admin users will also be able to review disputes and disagreements between students, as long as they are able to form an agreement with an Art admin. 

### 2.3.3 Art Student
Art students will be able to post jobs they wish for engineering students to complete for them. When they receive offers, they can choose which engineering student to choose for their job.

### 2.3.4 Engineering Student
Engineering students will be able to post jobs they wish for art students to complete for them. When they receive offers, they can choose which art student to choose for their job.

### 2.4 Operating Environment
The web application will be hosted on a web server. It will be accessible
through a web browser.

### 2.5 Design and Implementation Constraints
The web application will be written in Python using the Django framework. It
will be hosted on a web server. The data will be stored in a SQLite database.
No further design and implementation constraints are known at this time.

### 2.6 User Documentation
No documentation will be provided to users.

### 2.7 Assumptions and Dependencies
The application will use a virtual currency to handle payments. For the first version of the application, the amounts of "credits" each user have will be recorded as a field in the database and will be managed by admin users. The credits deposit and withdrawal funtionality will not be implemented in this version of the application. 

The application will not have an automated quality assurance process.

## 3. Specific Requirements

### 3.1 Functional Requirements

### 3.1.1 User Requirements

#### 3.1.1.1 User Registration

**Req U1** - The web application will allow users to register for an account. The web application will allow users to register for an account. Users will provide their name, email address, password, and type of student account (engineering or art). The web application will verify that the email address is unique and not already used by another user.

#### 3.1.1.2 User Login
**Req U2** - The web application will allow users to log in to their account.
The web application will allow users to log in to their account. Users will
provide their email address and password. The web application will verify that
the email address and password match the information in the database.

#### 3.1.1.3 User Logout
**Req U3** - The web application will allow users to log out of their account.
The web application will allow users to log out of their account. Users will
click on a "Log Out" button in the Navigation. The web application will
terminate the user's session.

#### 3.1.1.4 User Dashboard
**Req U4** - The web application will provide a dashboard for users to view
their account information and manage their jobs.
The web application will provide a dashboard for users to view their account
information and manage their jobs. The dashboard will include the following
information:

- User's name
- User's email address
- User's credit balance
- List of jobs posted by the user
- A list of quotes for jobs posted by the user
- A link to the student's profile for each quote
- A link to the student's profile for each accepted job

#### 3.1.1.5 User Profile
**Req U5** - The web application will provide a profile for users to view
and edit their account information.
The web application will provide a profile for users to view and edit their
account information. The profile will include the following information:

- User's name
- User's email address
- User's institution (university)
- User's programme (field of study)
- User's accepted jobs
- User's completed jobs
- A list of submitted bids
- A list of assigned jobs
- A list of completed jobs
- Students's rating

The user will be able to change their email address or their password on this
page.

#### 3.1.1.6 Job Posting
**Req U6** - The web application will allow users to post jobs. Users will provide the
following information:

Job title
Job description
Job budget

The web application will verify that the job title is unique and not already used by another job. The web application will verify that the job budget is a positive number. The web application will verify that the job category is relevant to the theme of the app (i.e. that an engineering job is posted by an art student and vice versa)

#### 3.1.1.7 Job Assignment
**Req U7** - The web application will allow users to accept a quote from a student and assign a job to the student. Users will click on a "Accept" button next to the quote. The web application will assign the job to the student and notify the student that the job has been assigned to them.

#### 3.1.1.8 Job Status
**Req U8** - The web application will allow users to view the status of their jobs. The web application will allow users to view the status of their jobs. Users will click on a "View Status" button next to the job. The web application will display the status of the job.

#### 3.1.1.9 Job Delivery
**Req U9** -  The web application will allow users to mark the job as finished upon finishing their tasks. Users will click on "Finish" button and will as such, complete the job.

#### 3.1.1.10 Job Review
**Req U10** - The web application will allow users to review the quality of completed tasks. Users will click on a "Review" button next to the job. The web application will display a form for the user to provide a review. The web application will verify that the review is between 1 and 5 stars. After the user submits the review, the web application will update the students's rating. The web application will calculate the new rating by calculating the average of all the reviews for the student. The web application will update the student's rating in the database. The web application will transfer the tokens for the job from the users balance to the students balance.

#### 3.1.1.11 Job listing
**Req U11** - The web application will allow users to view a list of jobs
posted by other users. Users will click on a "Jobs" button in the Navigation. The web application will display a list of jobs posted by other users. The list will only show unassigned jobs. The list will include the following information:

- Job title
- Job description
- Job field
- Job budget
- A link to the user's profile
- A "Bid on Job" button

#### 3.1.1.12 Job bidding
**Req U12** - The web application will allow users to bid on jobs posted by other users. The jobs will be listed in the same way as described in Req U11. Users will click on a "Bid on Job" button next to the job. The web application will display a form for the user to provide a quote. The web application will verify that the quote is a positive number. After the user submits the quote, the web application will send the quote to the user who posted the job. The web application will display the quote on the user's dashboard.

#### 3.1.1.13 Job Dispute
**Req U13**  - The web application will display a form for the user to provide a dispute. The web application will verify that the dispute is not empty. The dispute will be handled by the two administrators manually. The dispute has to contain the following information:

- Details about the job
- The user that posted the job
- The user that completed the job
- The reason for the dispute (provided by the user that posted the job)

### 3.1.2 Admin Requirements
Admin requirements focus on the specific funtionalities that Admins will be able to use. Admins are created by the system administrator in the Admin Dashboard. The Admins use the Django Admin interface to manage the system. This section applies both to Engineering and Art admins. 

#### 3.1.3.1 Admin Dashboard
**Req A1** - The web application will provide a dashboard for admins to manage the system. The dashboard will use the Django Admin interface. The dashboard will allow admins to manage the following:

- Users (create, edit, delete)
- Jobs (create, edit, delete)
- Apply a token balance to a user

#### 3.1.3.2 Dispute Resolution
**Req A2** - The web application will allow the admins to find a dispute in the Admin dashboard and to resolve the dispute. The dispute will be resolved by admin either suggesting his method, then waiting for the admin of the opposite qualifications, or accepting the proposed solution.

### 3.2 Usability Requirements
The web application will be designed to be easy to use. The web application will
be designed to be intuitive and easy to learn.

### 3.3 Reliability Requirements
None a this time

### 3.4 Performance Requirements
None a this time

### 3.5 Supportability Requirements
None a this time

### 3.6 Design Constraints
None a this time

## 4. System Interfaces

### 4.1 User Interfaces
The application will have an intuitive user interface that is accessed via the browser. The UI will be compatible with mobile and desktop devices. The web application will use a standard web interface with a top navigation bar and a main content area.

The top navigation bar will have the following links:
- On the left side:
    - Logo (link to the home page)
    - Jobs (link to the Job listing page)
    - Post a Job (link to the Post a Job page)
- On the right side:
    - Login (link to the Login page) if the user is not logged in
    - Profile (link to the Profile page) if the user is logged in
    - Logout (link to the Logout page) if the user is logged in
    - My Jobs (link to the My Jobs page) if the user is logged in


### 4.2 Hardware Interfaces
The application will be hosted on a secure web server running on Linux or Windows.

### 4.3 Software Interfaces
The application will use Django 5.0, the database will be SQLite, frontend framework will be Bootstrap.

### 4.4 Communications Interfaces
The application will have a secure communication interface for secure data transfer.

## 5. Other Nonfunctional Requirements

### 5.1 Security Requirements
The application will use the Django's default User management system. The
application will use Django's default authentication system. The application
will use Django's default authorization system. The application will use Django's
default password reset system.

### 5.2 Safety Requirements
The application must be designed to prevent the user from performing any unsafe actions or operations.

### 5.3 Business Rules
The application will have business rules in place to ensure the safety and security of user data.

### 5.4 Quality Attributes
The application must have a high quality user interface with good usability and responsiveness. The application must also have an uptime of at least 99.

## 6. Other Requirements

### 6.1 Business Requirements
None a this time

### 6.2 Regulatory Requirements
None a this time